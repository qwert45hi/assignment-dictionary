%include "colon.inc"
%include "words.inc"
%include "lib.inc"

%define BUF 256

global _start

extern find_word

section .rodata
prompt:     db  "Enter some key: ", 0
label:      db  "Found value is: ", 0
too_long:   db  "The key is too long", 0
not_found:  db  "404 Not Found", 0

section .text
_start:
    mov rdi, prompt
    call print_string

    sub rsp, BUF  ; stack -BUF
    mov rdi, rsp
    mov rsi, BUF
    call read_word
    test rax, rax
    jz .len_error

    mov rdi, rsp
    mov rsi, LAST_ENTRY
    push rdx
    call find_word
    add rsp, BUF  ; stack 0
    test rax, rax
    jz .key_error

    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    inc rax
    add rdi, rax

    push rdi
    mov rdi, label
    call print_string
    pop rdi
    jmp .end

    .len_error:
        add rsp, BUF  ; stack 0
        mov rdi, too_long
        jmp .end

    .key_error:
        mov rdi, not_found

    .end:
        call print_string
        call print_newline
        call exit
