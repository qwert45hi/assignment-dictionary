%macro colon 2

%ifid %2
    %ifdef LAST_ENTRY
        %2: dq LAST_ENTRY
    %else
        %2: dq 0
    %endif

    %define LAST_ENTRY %2
%else
    %fatal "Incorrect label"
%endif

%ifstr %1
    db %1, 0
%else
    %fatal "Key must be a string"
%endif

%endmacro
