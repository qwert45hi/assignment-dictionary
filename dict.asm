%include "lib.inc"

%define SZ 8

global find_word

section .text
; prams: rdi - string pointer, rsi - dict pointer
; return: addr of the dict entry if found else 0
find_word:
    test rsi, rsi
    jz .missing

    add rsi, SZ
    push rdi
    push rsi
    call string_equals
    pop rsi
    pop rdi

    test rax, rax
    jnz .success

    mov rsi, [rsi - SZ]
    jmp find_word

    .missing:
        xor rax, rax
        ret

    .success:
        mov rax, rsi
        sub rax, SZ
        ret
